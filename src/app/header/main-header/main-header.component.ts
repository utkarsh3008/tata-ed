import { CommonService } from './../../common.service';
import { AppConstant } from './../../app-constant';
import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-header',
  templateUrl: './main-header.component.html',
  styleUrls: ['./main-header.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainHeaderComponent implements OnInit {

  public isLogin = false;

  constructor(private _router: Router, private _commonService: CommonService, private _cdr: ChangeDetectorRef) { }

  ngOnInit() {
    this._commonService.isLoggedIn$.subscribe(result => {
      this.isLogin = result;
      this._cdr.markForCheck();
    })
  }

  public navigateToRevise() {
    this._router.navigate(["/revise"])
  }

  public logout() {
    localStorage.removeItem(AppConstant.localStorage.userData);
    this._router.navigate(["/signIn"]);
    this._commonService.isLoggedIn$.next(false);
  }
}
