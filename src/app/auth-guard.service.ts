import { CommonService } from "./common.service";
import { Injectable } from "@angular/core";
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router";
import { Observable } from "rxjs";
import { AppConstant } from "./app-constant";
import { UserData, TokenData } from "./account/account.types";

@Injectable()
export class AuthGuardService implements CanActivate {

    constructor(private _router: Router, private _commonService: CommonService) { }

    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
        if (this.isAuthorized(state.url)) {
            return true;
        }
        this._router.navigate(["/account/signIn"]);
        return false;
    }

    private isAuthorized(url: string): boolean {
        if (AppConstant.whitelistUrls.indexOf(url) !== -1 || this._commonService.isTokenValid()) {
            return true;
        }
        return false;
    }
}
