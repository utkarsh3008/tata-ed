import { DocumentData } from "@angular/fire/firestore";

export interface Activity {
    answer: string;
    options: string[];
    questionId: number;
    questionText: string;
    time: number;
    visited: boolean;
}

export interface StudyContent {
    activities: Activity[];
    offlineDownloadUrl: string;
    poster: string;
    title: string;
    url: string;
    docId: string;
}

export interface Activity {
    answer: string;
    options: string[];
    questionId: number;
    questionText: string;
    time: number;
    visited: boolean;
}

export interface VideoState {
    currentTime: number;
    maxTime: number;
    downloaded: boolean;
}

export interface UserStudyStateData {
    activities: Activity[];
    docId?: string;
    studyContentId: string;
    userId: string;
    videoState: VideoState;
}

export interface AppDownloadVideoData {
    docId: string;
    url: string;
}

