import { Activity } from "./../video.types";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Inject, Component } from "@angular/core";

@Component({
  selector: "app-question-dialog",
  templateUrl: "question-dialog.component.html",
})
export class QuestionDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<QuestionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public currentActivity: Activity) {
      dialogRef.disableClose = true;
     }

  submit(): void {
    this.dialogRef.close();
  }

}
