import { MatDialogModule, MatSelectionList, MatListModule } from "@angular/material";
import { MatCardModule } from "@angular/material/card";
import { VideoComponent } from "./video.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MatRadioModule } from "@angular/material/radio";
import { MatButtonModule } from "@angular/material/button";
import { VideoRoutingModule } from "./video-routing.module";
import { QuestionDialogComponent } from "./question-dialog/question-dialog.component";


@NgModule({
  imports: [
    CommonModule,
    VideoRoutingModule,
    MatButtonModule,
    MatCardModule,
    MatRadioModule,
    MatDialogModule,
    MatListModule
  ],
  declarations: [VideoComponent, QuestionDialogComponent],
  entryComponents: [QuestionDialogComponent]
})
export class VideoModule { }
