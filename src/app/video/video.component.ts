import { CommonService } from "./../common.service";
import { AppConstant } from "./../app-constant";
import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { take } from "rxjs/operators";
import { forkJoin } from "rxjs";
import { StudyContent, Activity, UserStudyStateData } from "./video.types";
import { VideoJsPlayer } from "video.js";
import { UserData } from "../account/account.types";
import { NetworkStatusAngularService } from "network-status-angular";
import { Router, ActivatedRoute } from "@angular/router";
import { MatDialog } from "@angular/material/dialog";
import { QuestionDialogComponent } from "./question-dialog/question-dialog.component";


declare let videojs: any;

@Component({
  selector: "app-video",
  templateUrl: "./video.component.html",
  styleUrls: ["./video.component.css"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VideoComponent implements OnInit, OnDestroy {

  public studyContent: StudyContent;
  public vidObj: VideoJsPlayer;
  public videoUrl: any = null;
  public title: string;
  public poster: any;
  public currentTime = 0;
  public currentActivity: Activity = null;
  public currentActivityIndex = 0;
  public userStateData: UserStudyStateData;
  public studyContentId: string;

  private serverVideoTime = 0;
  private disableFastForwardIntervalId: any;
  private androidAppLocalServerBaseUrl = "http://127.0.0.1:8080/";

  @ViewChild("myvid") vid: ElementRef;

  constructor(
    private _afs: AngularFirestore,
    private _cdr: ChangeDetectorRef,
    private _networkStatusAngularService: NetworkStatusAngularService,
    private _route: ActivatedRoute,
    public dialog: MatDialog,
    private _commonService: CommonService
  ) {
  }

  ngOnInit() {
    this.studyContentId = this._route.snapshot.params.id;

    const options = {
      controls: true,
      autoplay: false,
      preload: "auto",
      techOrder: ["html5"],
      width: window.innerWidth - 48
    };
    this.vidObj = new videojs(this.vid.nativeElement, options, function onPlayerReady() {
      videojs.log("Your player is ready!");
    });


    this.fetchData();
    this._networkStatusAngularService.status.subscribe(status => {
      console.log("status", status);

      this.fetchData();
      this._cdr.markForCheck();
    });

    this._commonService.onVideoDownloaded$.subscribe((data) => {
      console.log("video downloaded", data);
      this._afs.collection(`userStudyData`).doc(data.docId).update({
        "videoState.downloaded": true
      }).then(res => console.log(" videoState.downloaded updated"));
    });
  }

  public openDialog(): void {
    const dialogRef = this.dialog.open(QuestionDialogComponent, {
      data: this.currentActivity
    });

    dialogRef.afterClosed().subscribe(() => {
      this.submit();
    });
  }

  private fetchData(isChangeVideoSource = true) {
    const studyContent$ = this._afs.collection("studyContent").doc(this.studyContentId).get();
    const userStudyData$ = this._afs.collection(this._afs.collection("userStudyData").ref, ref => {
      return ref.where("userId", "==", this.getUserData().uid).where("studyContentId", "==", this.studyContentId);
    })
      .get();
    forkJoin([studyContent$, userStudyData$]).pipe(
      take(1)
    ).subscribe(result => {
      const snapshotStudyContent = result[0];
      this.studyContent = snapshotStudyContent.data() as StudyContent;
      this.studyContent.docId = snapshotStudyContent.id;
      this.disableFastForward();

      const snapshotUserStudyData = result[1];
      if (snapshotUserStudyData.docs.length > 0) {
        const stateData = snapshotUserStudyData.docs[0].data();
        this.userStateData = stateData as UserStudyStateData;
        this.userStateData.docId = snapshotUserStudyData.docs[0].id;

        if (!this.userStateData.activities) {
          this.userStateData.activities = this.studyContent.activities;
        }


        if (stateData.videoState && (stateData.videoState.currentTime || stateData.videoState.maxTime)) {
          if (stateData.videoState.maxTime) {
            this.serverVideoTime = stateData.videoState.maxTime;
          }


          if (stateData.videoState.currentTime) {
            this.currentTime = stateData.videoState.currentTime;
            this.vidObj.currentTime(stateData.videoState.currentTime);
          }
        }
      } else {
        this.userStateData = {
          userId: this.getUserData().uid,
          studyContentId: this.studyContent.docId,
          videoState: {
            currentTime: 0,
            maxTime: 0,
            downloaded: false
          },
          activities: this.studyContent.activities
        };
      }

      if (isChangeVideoSource) {
        if (navigator.onLine) {
          this.vidObj.src({ type: "video/mp4", src: this.studyContent.url });
        } else {
          if (this.userStateData.videoState.downloaded) {
            const videoName = this.studyContent.url.substring(this.studyContent.url.lastIndexOf("/") + 1);
            const videoUrl = `${this.androidAppLocalServerBaseUrl}${videoName}`;
            this.vidObj.src({ type: "video/mp4", src: videoUrl });
          }
          else {
            this.vidObj.error("You are offline and video not downloaded for offline play");
          }
        }
        setTimeout(() => {
          this.fetchData(false);
        });
      }
      this._cdr.markForCheck();
    });
  }

  public downloadVideo() {
    (window as any).androidObj.requestForDownloadVideo(JSON.stringify({ docId: this.userStateData.docId, url: this.studyContent.url }));
  }


  private getUserData() {
    const userStr = localStorage.getItem(AppConstant.localStorage.userData);
    if (userStr) {
      const user: UserData = JSON.parse(userStr);
      return user;
    }
    return null;
  }

  private disableFastForward() {
    this.vidObj.on("seeking", () => {
      if (this.currentTime < this.vidObj.currentTime()) {
        if (this.serverVideoTime < this.vidObj.currentTime()) {
          this.vidObj.currentTime(this.currentTime);
        }
      }
    });

    this.vidObj.on("seeked", () => {
      if (this.currentTime < this.vidObj.currentTime()) {
        if (this.serverVideoTime < this.vidObj.currentTime()) {
          this.vidObj.currentTime(this.currentTime);
        }
      }
    });

    this.disableFastForwardIntervalId = setInterval(() => {
      // Demo comment
      if (this.vidObj.player_ !== null) {

        if (!this.vidObj.paused() && this.currentTime < this.vidObj.currentTime()) {
          this.currentTime = this.vidObj.currentTime();
        }

        if (!this.vidObj.paused() && parseInt(this.vidObj.currentTime().toString(), 10) % 2 === 0) {
          this.updateVideoStateData();
        }
        this.showActivity();
      }
    }, 1000);

  }


  public showActivity() {
    if (this.studyContent && this.studyContent.activities) {
      this.userStateData.activities.forEach((activity, index) => {
        if (!this.vidObj.paused() && parseInt(this.vidObj.currentTime().toString(), 10) === activity.time && activity.visited === false) {
          this.vidObj.pause();
          this.currentActivityIndex = index;
          this.currentActivity = activity;
          this.openDialog();
          this._cdr.markForCheck();
        }
      });
    }
  }

  private updateVideoStateData() {
    if (this.vidObj.currentTime() > this.serverVideoTime) {
      this.userStateData.videoState.maxTime = this.vidObj.currentTime();
      this.userStateData.videoState.currentTime = this.vidObj.currentTime();
      this.serverVideoTime = this.vidObj.currentTime();
      this._afs.collection(`userStudyData`).doc(this.getUserStudyDataDocId()).set(this.userStateData, { merge: true });
    } else {
      this.userStateData.videoState.currentTime = this.vidObj.currentTime();
      this._afs.collection(`userStudyData`).doc(this.getUserStudyDataDocId()).set(this.userStateData, { merge: true });
    }
  }

  private getUserStudyDataDocId() {
    return `${this.getUserData().uid}_${this.studyContent.docId}`;
  }

  public submit() {
    this.currentActivity.visited = true;
    this.userStateData.activities[this.currentActivityIndex].visited = true;
    this._afs.collection("userStudyData").doc(this.userStateData.docId).set(this.userStateData, { merge: true }).then();
    this.vidObj.play();
    this.currentActivity = null;
    this._cdr.markForCheck();
  }

  public ngOnDestroy() {
    clearInterval(this.disableFastForwardIntervalId);
    this.vidObj.dispose();
  }
}
