import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { VideoComponent } from "./video.component";

const routes: Routes = [
    {
        path: ":id",
        component: VideoComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class VideoRoutingModule { }
