import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { ReviseComponent } from "./revise.component";

const routes: Routes = [
    {
        path: "",
        component: ReviseComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class ReviseRoutingModule { }
