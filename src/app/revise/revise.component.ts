import { StudyContent } from "./../video/video.types";
import { take } from "rxjs/operators";
import { Component, OnInit } from "@angular/core";
import { MatButtonModule } from "@angular/material/button";
import { Router } from "@angular/router";
import { AngularFirestore } from "@angular/fire/firestore";


@Component({
  selector: "app-revise",
  templateUrl: "./revise.component.html",
  styleUrls: ["./revise.component.css"]
})
export class ReviseComponent implements OnInit {
  public studyContent: StudyContent;
  public myData: any = [];

  constructor(private _router: Router,
    private _afs: AngularFirestore) { }

  ngOnInit() {
    const studyContent$ = this._afs.collection("studyContent").get().pipe(take(1)).subscribe(result => {
      const studyContentData = result;
      studyContentData.docs.forEach(doc => {
        this.studyContent = doc.data() as StudyContent;
        this.studyContent.docId = doc.id;
        this.myData.push(this.studyContent);
      });
    });
  }

  public revise(id: any) {
    console.log(id, "asdasd");
    this._router.navigate([`/video/${id}`]);
  }

}
