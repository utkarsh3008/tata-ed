import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReviseComponent } from "./revise.component";
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { ReviseRoutingModule } from "./revise-routing.module";



@NgModule({
  imports: [
    CommonModule,
    ReviseRoutingModule,
    MatButtonModule,
    MatCardModule
  ],
  declarations: [ReviseComponent]
})
export class ReviseModule { }
