import { ReviseComponent } from './revise/revise.component';
import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { auth } from 'firebase/app';
import { AngularFireAuth } from "@angular/fire/auth";
import { User } from 'firebase';
import { from, of } from "rxjs";
import { catchError } from "rxjs/operators";
import { LoginWithEmailPassword } from './account/account.types';


@Injectable({
    providedIn: 'root'
})
export class AuthService {
    user: User;

    constructor(private afAuth: AngularFireAuth,
        private router: Router,) {

    }

    public login(credential: LoginWithEmailPassword) {
        return from(this.afAuth.auth.signInWithEmailAndPassword(credential.email, credential.password)).pipe(
            catchError(error => {
                console.log("error", error);
                return of(false);
            })
        )
    }

    get isLoggedIn(): boolean {
        const user = JSON.parse(localStorage.getItem('user'));
        return user !== null;
    }
}