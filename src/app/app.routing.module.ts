import { VideoComponent } from "./video/video.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuardService } from "./auth-guard.service";


const routes: Routes = [
    {
        path: "revise",
        loadChildren: "./revise/revise.module#ReviseModule",
        canActivate: [AuthGuardService]
    },
    {
        path: "account",
        loadChildren: "./account/account.module#AccountModule"
    },
    {
        path: "video",
        loadChildren: "./video/video.module#VideoModule",
        canActivate: [AuthGuardService]
    },
    {
        path: "**",
        redirectTo: "account/signIn",
        pathMatch: "full"
    }
];
@NgModule({
    imports: [
        RouterModule.forRoot(routes, { enableTracing: false })
    ],
    exports: [
        RouterModule
    ],
    declarations: []
})
export class AppRoutingModule { }
