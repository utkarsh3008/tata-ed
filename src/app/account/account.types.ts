export interface LoginWithEmailPassword {
    email: string;
    password: string;
}


export interface ProviderData {
    uid: string;
    displayName?: any;
    photoURL?: any;
    email: string;
    phoneNumber?: any;
    providerId: string;
}

export interface StsTokenManager {
    apiKey: string;
    refreshToken: string;
    accessToken: string;
    expirationTime: number;
}

export interface UserData {
    uid: string;
    displayName?: any;
    photoURL?: any;
    email: string;
    emailVerified: boolean;
    phoneNumber?: any;
    isAnonymous: boolean;
    providerData: ProviderData[];
    apiKey: string;
    appName: string;
    authDomain: string;
    stsTokenManager: StsTokenManager;
    redirectEventId?: any;
    lastLoginAt: string;
    createdAt: string;
}


export interface Identities {
    email: string[];
}

export interface Firebase {
    identities: Identities;
    sign_in_provider: string;
}

export interface TokenData {
    iss: string;
    aud: string;
    auth_time: number;
    user_id: string;
    sub: string;
    iat: number;
    exp: number;
    email: string;
    email_verified: boolean;
    firebase: Firebase;
}
