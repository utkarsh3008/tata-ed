import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SignInComponent } from "./sign-in/sign-in.component";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatFormFieldModule } from "@angular/material/form-field";
import { RouterModule } from "@angular/router";
import { MatInputModule, MatButtonModule } from "@angular/material";
import { MatCardModule } from "@angular/material/card";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AccountRoutingModule } from "./account.routing";

@NgModule({
  imports: [
    CommonModule,
    AccountRoutingModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatButtonModule,
    RouterModule,
    MatInputModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [SignInComponent]
})
export class AccountModule { }
