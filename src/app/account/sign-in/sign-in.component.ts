import { CommonService } from "./../../common.service";
import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { MatDialog } from "@angular/material";
import { Router } from "@angular/router";
import { AuthService } from "./../../auth.service";
import { FormGroup, FormControl, FormBuilder, Validators } from "@angular/forms";
import { AppConstant } from "../../app-constant";
import { AngularFirestore } from "@angular/fire/firestore";

@Component({
  selector: "app-sign-in",
  templateUrl: "./sign-in.component.html",
  styleUrls: ["./sign-in.component.css"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SignInComponent implements OnInit {
  public loginForm: FormGroup;

  constructor(
    private _router: Router,
    private _authService: AuthService,
    private _fb: FormBuilder,
    private _afs: AngularFirestore,
    private _commonService: CommonService
  ) {
    this.loginForm = this._fb.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required]]
    });
  }

  ngOnInit() {
    if (this._commonService.isTokenValid()) {
      this._router.navigate(["/revise"]);
    }
  }

  login(): void {
    if (this.loginForm.valid) {
      this._authService.login(this.loginForm.value).subscribe(result => {
        if (typeof result !== "boolean") {

          if (result && result.user) {
            localStorage.setItem(AppConstant.localStorage.userData, JSON.stringify(result.user.toJSON()));
            this._router.navigate(["/revise"]);
            console.log("resultUser", result.user);
            this._commonService.isLoggedIn$.next(true)

          }

        }
      });
    }
  }
}
