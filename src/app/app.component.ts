import { CommonService } from "./common.service";
import { AppConstant } from "./app-constant";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AngularFirestore } from "@angular/fire/firestore";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  title = "app";

  constructor(private _router: Router, private _afs: AngularFirestore, private _commonService: CommonService) { }

  public async ngOnInit() {
    await this._afs.firestore.enablePersistence();

    const onVideoDownloadedFn = (studyContent: string) => {
      this._commonService.onVideoDownloaded$.next(JSON.parse(studyContent));
    };
    if ((window as any).androidObj) {
      console.log("androidObj already exist");
      (window as any).androidObj.onVideoDownloaded = onVideoDownloadedFn;
    } else {
      console.log("androidObj created");
      (window as any).androidObj = {
        onVideoDownloaded: onVideoDownloadedFn
      };
    }

    this._commonService.isLoggedIn$.next(this._commonService.isTokenValid());
  }
}
