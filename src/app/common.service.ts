import { AppDownloadVideoData } from "./video/video.types";
import { AppConstant } from "./app-constant";
import { UserData, TokenData } from "./account/account.types";
import { Injectable } from "@angular/core";
import { Subject, BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class CommonService {
    public onVideoDownloaded$ = new Subject<AppDownloadVideoData>();
    public isLoggedIn$ = new BehaviorSubject<boolean>(false);

  constructor() { }

  public  isTokenValid() {
    const userDataStr = localStorage.getItem(AppConstant.localStorage.userData);
    if (userDataStr) {
        const userData: UserData = JSON.parse(userDataStr);
        const jwtToken = userData.stsTokenManager.accessToken;
        const tokenDataStr = atob(jwtToken.split(".")[1]);
        const tokenData: TokenData = JSON.parse(tokenDataStr);
        if (tokenData.exp * 1000 > Date.now()) {
            return true;
        }
    }
    return false;
}
}
