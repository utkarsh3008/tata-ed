import { CommonService } from "./common.service";
import { MatButtonModule } from "@angular/material/button";
import { AppRoutingModule } from "./app.routing.module";
import { environment } from "./../environments/environment";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { MatCardModule } from "@angular/material/card";
import {MatRadioModule} from "@angular/material/radio";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatFormFieldModule } from "@angular/material/form-field";
import { AngularFireModule } from "@angular/fire";
import { AppComponent } from "./app.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatInputModule } from "@angular/material";
import { AngularFirestore } from "@angular/fire/firestore";
import { ServiceWorkerModule } from "@angular/service-worker";
import { NetworkStatusAngularModule } from "network-status-angular";
import { AuthGuardService } from "./auth-guard.service";
import { MainHeaderComponent } from "./header/main-header/main-header.component";

console.log(environment.ngswPath);
@NgModule({
  declarations: [
    AppComponent,
    MainHeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatCardModule,
    MatRadioModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    NetworkStatusAngularModule.forRoot(),
    ServiceWorkerModule.register(environment.ngswPath, { enabled: environment.production })

  ],
  providers: [AngularFirestore, AuthGuardService, CommonService],
  bootstrap: [AppComponent]
})
export class AppModule { }
