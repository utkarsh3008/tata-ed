// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
    firebaseConfig : {
    apiKey: "AIzaSyCwdnER76y1ptxa7iVsV9TbsAXgdK3sK3c",
    authDomain: "tata-edge-bc300.firebaseapp.com",
    databaseURL: "https://tata-edge-bc300.firebaseio.com",
    projectId: "tata-edge-bc300",
    storageBucket: "tata-edge-bc300.appspot.com",
    messagingSenderId: "1045369148105",
    appId: "1:1045369148105:web:b760cd37e2a69296823318",
    measurementId: "G-9XV2DMY82C"
  },
  ngswPath: "../../ngsw-worker.js"
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
