export const environment = {
  production: true,
   firebaseConfig : {
    apiKey: "AIzaSyCwdnER76y1ptxa7iVsV9TbsAXgdK3sK3c",
    authDomain: "tata-edge-bc300.firebaseapp.com",
    databaseURL: "https://tata-edge-bc300.firebaseio.com",
    projectId: "tata-edge-bc300",
    storageBucket: "tata-edge-bc300.appspot.com",
    messagingSenderId: "1045369148105",
    appId: "1:1045369148105:web:b760cd37e2a69296823318",
    measurementId: "G-9XV2DMY82C"
  },
  ngswPath: "/ngsw-worker.js"
};
